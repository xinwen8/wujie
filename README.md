## 苹果版 无界VPN 1.0.1 (支持 iPhone 5S 以上)

#### 安装：

如果您有国外苹果账号，可以直接安装：
https://apps.apple.com/us/app/ultrasurf-vpn/id1563051300

如果没有，先在苹果机上点击并安装 TestFlight 软件
https://apps.apple.com/us/app/testflight/id899247664

然后点击以下链接安装
https://testflight.apple.com/join/HGroNaNq

#### 功能与使用：

1. 开启后，轻触或滑动开关，显示“正在连接 ..."，同时时上面会出现vpn标志。
2. 连接成功后显示“连接成功“。此时您可以使用任何浏览器或app，都在无界加密保护下。
5. 使用时，只要vpn标志在，就在在无界加密保护下。
6. 如果要停止使用，轻触或滑动开关即可。关闭后，vpn标志会消失，这时手机直接联网，不在无界加密保护下。
7. 如果问题，可关闭再开启或重启手机再运行无界。
